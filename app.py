import asyncio
import datetime
import random
import sys

import redis


DEBUG = False
GEN_SEND_MESSAGE_SECONDS = 0.5
ERROR_MESSAGE_CHANCE = 0.05


# For run with param getErrors
def get_errors():
    errors = r.getset('errors', '')
    if errors:
        print(errors.decode('utf8'))
    else:
        print('No errors')


# Message functions

# For random message
stdout = sys.stdout
sys.stdout = type('EatStdout', (), {'write': (lambda self, string: '')})()
from this import d, s
sys.stdout = stdout


def random_message():
    zen_of_python = [line for line in "".join([d.get(c, c) for c in s]).split('\n') if line != '']
    return '[%(datetime)s] %(zen_line)s' % {'datetime': datetime.datetime.now(), 'zen_line': random.choice(zen_of_python)}


async def send_message():
    message = random_message()
    r.rpush('messages', message)
    if DEBUG:
        print('Send message - %s' % message)
    await asyncio.sleep(GEN_SEND_MESSAGE_SECONDS)


async def handle_message():
    message = r.lpop('messages')
    if message:
        if DEBUG:
            print('Handle %s' % message)
        if random.random() < ERROR_MESSAGE_CHANCE:
            if DEBUG:
                print('Error handle message - %s' % message)
            r.append('errors', message + b'\n')
    await asyncio.sleep(0.1)


# Generator functions
async def i_am_here():
    r.pexpire('generator', 500)
    await asyncio.sleep(0.250)


async def generator_check():
    global me_is_generator
    existing_generator = r.get('generator')
    if not existing_generator:
        with r.pipeline() as pipe:
            try:
                pipe.multi()
                pipe.psetex('generator', 1000, 'i_am_here')
                pipe.execute()
                me_is_generator = True
                if DEBUG:
                    print('I am generator')
            except redis.WatchError:
                if DEBUG:
                    print('Failure to become a generator')
                me_is_generator = False
            finally:
                pipe.reset()
    await asyncio.sleep(0.1)


# Async task
async def message_service():
    while True:
        if me_is_generator:
            await send_message()
        else:
            await handle_message()


async def generator_control_service():
    while True:
        if me_is_generator:
            await i_am_here()
        else:
            await generator_check()


r = redis.StrictRedis(host='localhost', port=6379, db=0)


args = dict(zip(['file', 'mode'], sys.argv))
MODE = args.get('mode', 'to_battle')

if MODE == 'debug':
    DEBUG = True
    MODE = 'to_battle'

if MODE == 'to_battle':
    me_is_generator = False
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(
        generator_control_service(),
        message_service(),
    ))
    loop.close()

if MODE == 'getErrors':
    get_errors()
